package com.dzung.thirtydays;

import java.util.Scanner;

class BSTNode {
	BSTNode left,right;
  int data;
  BSTNode(int data){
      this.data=data;
      left=right=null;
  }
}

class BSTHeight {
	
	public static int getHeight(BSTNode root){
    //Write your code here
    int height = findHeight(root);
    return height == 0 ? 0 : height - 1;
  }
	
	private static int findHeight(BSTNode root) {
		if (root == null) {
    	return 0;
    }
    int lheight = findHeight(root.left);
    int rheight = findHeight(root.right);
    if (lheight > rheight) {
    	return lheight + 1;
    } else {
    	return rheight + 1;
    }
	}
	
	public static BSTNode insert(BSTNode root,int data){
    if(root==null){
        return new BSTNode(data);
    }
    else{
    	BSTNode cur;
        if(data<=root.data){
            cur=insert(root.left,data);
            root.left=cur;
        }
        else{
            cur=insert(root.right,data);
            root.right=cur;
        }
        return root;
    }
}
public static void main(String args[]){
    Scanner sc=new Scanner(System.in);
    int T=sc.nextInt();
    BSTNode root=null;
    while(T-->0){
        int data=sc.nextInt();
        root=insert(root,data);
    }
    int height=getHeight(root);
    System.out.println(height);
}
	
}