package com.dzung.thirtydays;

import java.util.*;
import java.math.*;

public class Arithmetic {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double mealCost = scan.nextDouble();
		int tipPercent = scan.nextInt();
		int taxPercent = scan.nextInt();
		
		double totalCostDouble = mealCost + mealCost * (tipPercent + taxPercent) / 100;
		int totalCost = (int) Math.round(totalCostDouble);
		
		System.out.printf("The total meal cost is %d dollars.", totalCost);
	}
	
}
