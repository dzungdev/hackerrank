package com.dzung.thirtydays;

import java.io.*;
import java.util.*;

public class LinkedList {

	public static Node insert(Node head, int data) {
		if (head == null) {
			return new Node(data);
		}
		Node tail = head;
		while (tail.next != null) {
			tail = tail.next;
		}
		Node newNode = new Node(data);
		tail.next = newNode;

		return head;
	}

	public static void display(Node head) {
		Node start = head;
		while (start != null) {
			System.out.print(start.data + " ");
			start = start.next;
		}
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		Node head = null;
		int N = sc.nextInt();

		while (N-- > 0) {
			int ele = sc.nextInt();
			head = insert(head, ele);
		}
		display(head);
		
		Stack stack = new Stack<>();
		
		int[] arr = null;
		Arrays.stream(arr).forEach(System.out::print);
		sc.close();
	}

}

class Node {
	int		data;
	Node	next;

	Node(int d) {
		data = d;
		next = null;
	}
}