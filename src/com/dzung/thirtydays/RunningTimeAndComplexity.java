package com.dzung.thirtydays;

import java.util.Scanner;

public class RunningTimeAndComplexity {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
    int noOfTestcase = scan.nextInt();
    while (scan.hasNextInt()) {
        int num = scan.nextInt();
        if (num == 1) {
        	System.out.println("Prime");
        	continue;
        }
        int maxIndex = (int)Math.sqrt(num) + 1;
        boolean isPrime = true;
        for(int i = 2; i < maxIndex; i++) {
            if (num % i == 0) {
            	isPrime = false;
              break;
            }
        }
        if (isPrime) {
        	System.out.println("Prime");
        } else {
        	System.out.println("Not prime");
        }
    }
	}
	
}