package com.dzung.thirtydays;

import java.util.Scanner;

public class Scope {
	
	
	
	public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int[] a = new int[n];
    for (int i = 0; i < n; i++) {
        a[i] = sc.nextInt();
    }
    sc.close();

    Difference difference = new Difference(a);

    difference.computeDifference();

    System.out.print(difference.maximumDifference);
}
}

class Difference {
	public int[] elements;
	public int maximumDifference;
	
	
	
// Add your code here
  public Difference(int[] elements) {
      this.elements = elements;
  }
  
  private static void merge(int[] arr, int[] left, int[] right) {
		int i = 0, j = 0, k=0;
		while (i < left.length && j < right.length ) {
			if (left[i] <= right[j]) {
				arr[k] = left[i];
				i++;
			} else {
				arr[k] = right[j];
				j++;
			}
			k++;
		}
		// in the case right and left finish first, we will need to loop
		// the rest of the array which still have remaining element to put
		// into arr
		while (i < left.length) {
			arr[k] = left[i];
			k++;i++;
		}
		while (j < right.length) {
			arr[k] = right[j];
			k++;j++;
		}
	}
	
	/**
	 * * we will use recursion to make merge sort for left and right array
	 * until the length of array is 1
	 * 1. based case is arr length smaller than 2
	 * 2. create left and right arr
	 * 3. run mergeSort for left
	 * 4. run mergeSort for right
	 * 5. run merge for left and right arr
	 * @param  arr [description]
	 * @return     [description]
	 */
	public static void mergeSort(int[] arr) {
		if (arr.length < 2) return;
		int mid = arr.length / 2;
		int[] left = new int[mid];
		int[] right = new int[arr.length - mid];
		for (int i = 0; i < mid; i++) {
			left[i] = arr[i];
		}
		for (int i = mid; i < arr.length; i++) {
			right[i - mid] = arr[i];
		}
		mergeSort(left);
		mergeSort(right);
		merge(arr, left, right);
	}

  public int computeDifference() {
  		mergeSort(this.elements);
      return this.maximumDifference;
  }
  
  public static int maxDiff(int arr[], int arr_size) 
  {
      int max_diff = arr[1] - arr[0];
      int min_element = arr[0];
      int i;
      for (i = 1; i < arr_size; i++) 
      {
          if (arr[i] - min_element > max_diff)
              max_diff = arr[i] - min_element;
          if (arr[i] < min_element)
              min_element = arr[i];
      }
      return max_diff;
  }
}
