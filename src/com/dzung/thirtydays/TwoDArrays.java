package com.dzung.thirtydays;

import java.util.Scanner;

public class TwoDArrays {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
    int arr[][] = new int[6][6];
    for(int i=0; i < 6; i++){
        for(int j=0; j < 6; j++) {
        	arr[i][j] = in.nextInt();
        }
    }
    int[][] maxHourglass = new int[3][3];
    long max = Long.MIN_VALUE;
    for (int i = 0; i < 4; i++) {
    	for (int j = 0; j < 4; j++) {
    		long hourglassTotal = arr[i][j] + arr[i][j+1] + arr[i][j+2] +
    				arr[i+1][j+1] + arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2]; 
    		if (hourglassTotal > max) {
    			maxHourglass[0][0] = arr[i][j];
    			maxHourglass[0][1] = arr[i][j+1];
    			maxHourglass[0][2] = arr[i][j+2];
    			maxHourglass[1][1] = arr[i+1][j+1];
    			maxHourglass[2][0] = arr[i+2][j];
    			maxHourglass[2][1] = arr[i+2][j+1];
    			maxHourglass[2][2] = arr[i+2][j+2];
    			max = hourglassTotal;
    		}
    	}
    }
    System.out.println(max);
    for (int i = 0; i < 3; i++) {
    	if (i == 1) {
   		 System.out.println("  " + maxHourglass[i][1] + "  ");
   		} else {
   			System.out.println(maxHourglass[i][0] + " " + maxHourglass[i][1] + " " + maxHourglass[i][2]);
   		}
    }
	}
}
