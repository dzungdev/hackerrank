package com.dzung.thirtydays;

import java.util.Scanner;

public class LetReviewDaySixth {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		scan.nextLine();
		while (scan.hasNextLine()) {
			String line = scan.nextLine();
			String evenString = "";
			String oddString = "";
			char[] arr = line.toCharArray();
			for(int i = 0; i < arr.length; i++) {
				if (i % 2 == 0) {
					evenString += String.valueOf(arr[i]);
				} else {
					oddString += String.valueOf(arr[i]);
				}
			}
			System.out.println(evenString + " " + oddString); 
		}
	}
}
