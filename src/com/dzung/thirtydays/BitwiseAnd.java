package com.dzung.thirtydays;

import java.util.Scanner;
import java.util.stream.IntStream;

public class BitwiseAnd {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		int testCaseNum = scan.nextInt();
		while (testCaseNum > 0) {
			int n = scan.nextInt();
			int k = scan.nextInt();
			int max = 0;
			
			int[] a = IntStream.range(1, n+1).toArray();
			for (int i = 0; i < n; i++) {
				for (int j = i+1; j < n; j++) {
					int tempMax = a[i] & a[j];
					if (tempMax > max && tempMax < k) max = tempMax;
				}
			}
			System.out.println(max);
			
			//base case to stop
			testCaseNum--;
		}
		scan.close();
	}
}
