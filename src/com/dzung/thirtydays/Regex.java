package com.dzung.thirtydays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Regex {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
    int num = scan.nextInt();
    List<String> firstNames = new ArrayList<>(); 
    for (int i = 0; i < num; i++) {
        String firstName = scan.next();
        String email = scan.next();
        
        boolean isValidFirstName = Pattern.matches("[a-z]+", firstName);
        boolean isValidEmail = Pattern.matches("[a-z]+@[a-z]*gmail.com", email);
        if (isValidFirstName && isValidEmail) firstNames.add(firstName);
    }
    Collections.sort(firstNames);
    firstNames.forEach(System.out::println);
    scan.close();
	}
}
