package com.dzung.thirtydays;

public class Student {
	
	int[] testScores;
	
	public Student(int[] scores) {
		this.testScores = scores;
	}
	
	public char calculate() {
    double average = 0;
    for (int score: this.testScores) {
        average += score;
    }
    average = (double) average/this.testScores.length;
    char grade = ' ';
    if (average <= 100 && average >= 90) {
        grade = 'O';
    } else if (average < 90 && average >= 80) {
        grade = 'E';
    } else if (average < 80 && average >= 70) {
        grade = 'A';
    } else if (average < 70 && average >= 55) {
        grade = 'P';
    } else if (average < 55 && average >= 40) {
        grade = 'D';
    } else {
        grade = 'T';
    }
    return grade;
}
}
