package com.hackkerrank.algorithm.dp;

import java.math.BigInteger;
import java.util.Scanner;

public class ModifiedFibonacci {
	
	public static BigInteger modifiedFibonacci(int i, BigInteger t1, BigInteger t2) {
		if (i < 3) {
			if (i == 2) return  t2;
			if (i == 1) return  t1;
			return new BigInteger("0");
		} else {
			return modifiedFibonacci(i - 2, t1, t2).add( 
																									modifiedFibonacci(i -1, t1, t2).multiply(modifiedFibonacci(i - 1, t1, t2)));
		}
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		BigInteger t1 = scan.nextBigInteger();
		BigInteger t2 = scan.nextBigInteger();
		int n = scan.nextInt();
		System.out.print(modifiedFibonacci(n, t1, t2));
		scan.close();
	}
	
}
