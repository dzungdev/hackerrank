package com.hackkerrank.algorithm.warmup;

import java.util.Scanner;

public class MiniMaxSum {
	
	private static long[] insertionSort(long[] arr) {
		for (int i = 1; i < arr.length; i++) {
			long curVal = arr[i];
			int reverseIndex = i;
			while (reverseIndex > 0 && arr[reverseIndex - 1] > curVal) {
				arr[reverseIndex] = arr[reverseIndex - 1];
				reverseIndex--;
			}
			arr[reverseIndex] = curVal;
		}
		return arr;
	}
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
    long a = in.nextLong();
    long b = in.nextLong();
    long c = in.nextLong();
    long d = in.nextLong();
    long e = in.nextLong();
    
    long[] arr = new long[] {a,b,c,d,e};
    //sort the arr
    arr = insertionSort(arr);
    
    long minVal = arr[0] + arr[1] + arr[2] + arr[3];
    int arrLength = arr.length;
    long maxVal = arr[arrLength - 1] + arr[arrLength - 2] + arr[arrLength - 3] + arr[arrLength - 4];
    System.out.print(minVal + " " + maxVal);
    in.close();
	}
}
