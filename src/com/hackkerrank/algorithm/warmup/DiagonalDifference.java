package com.hackkerrank.algorithm.warmup;

import java.util.Scanner;

public class DiagonalDifference {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int[][] arr = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j =0; j < n; j++) {
				arr[i][j] = scan.nextInt();
			}
		}
		int sumLeftDiagonal = 0;
		int sumRightDiagonal = 0;
		for (int i = 0; i < n; i++) {
			sumLeftDiagonal += arr[i][i];
		}
		
		for (int i = 0; i < n; i++) {
			sumRightDiagonal += arr[n - 1 - i][i];
		}
		
		System.out.print(Math.abs(sumLeftDiagonal - sumRightDiagonal));
		
		scan.close();
	}
}
