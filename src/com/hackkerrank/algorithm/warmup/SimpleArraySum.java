package com.hackkerrank.algorithm.warmup;

import java.io.*;
import java.util.*;

public class SimpleArraySum {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numsSize = scan.nextInt();
        int total = 0;
        while (scan.hasNext()) {
        	total += scan.nextInt();
        }
        System.out.print(total);
    }
}