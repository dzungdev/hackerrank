package com.hackkerrank.algorithm.warmup;

import java.util.Scanner;

public class ComparingTriplet {

	static void compare(int a, int b, int[] arr) {
		if (a < b) {
			arr[1] += 1;
		} else if (a > b) {
			arr[0] += 1;
		}
	}

	static int[] solve(int a0, int a1, int a2, int b0, int b1, int b2) {
		int[] arr = new int[] { 0, 0 };
		compare(a0, b0, arr);
		compare(a1, b1, arr);
		compare(a2, b2, arr);
		return arr;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int a0 = in.nextInt();
		int a1 = in.nextInt();
		int a2 = in.nextInt();
		int b0 = in.nextInt();
		int b1 = in.nextInt();
		int b2 = in.nextInt();
		int[] result = solve(a0, a1, a2, b0, b1, b2);
		String separator = "", delimiter = " ";
		for (Integer value : result) {
			System.out.print(separator + value);
			separator = delimiter;
		}
		System.out.println("");

	}

}
