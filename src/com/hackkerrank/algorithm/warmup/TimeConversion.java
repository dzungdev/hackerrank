package com.hackkerrank.algorithm.warmup;

import java.util.Scanner;

public class TimeConversion {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		String hour = scan.nextLine();
		String clockFormat = hour.substring(hour.length() - 2);
		String hour12 = hour.substring(0, hour.length() - 2);
		String[] hours = hour12.split(":");
		if ("PM".equals(clockFormat)) {
			if (!"12".equals(hours[0])) {
				int hour24format = Integer.parseInt(hours[0]) + 12;
				hour12 = hour12.replace(hours[0], String.valueOf(hour24format));
			}
		} else {
			if ("12".equals(hours[0])) {
				hour12 = hour12.replace(hours[0], "00");
			}
		}
		
		System.out.println(hour12);
		
		scan.close();
	}
}
