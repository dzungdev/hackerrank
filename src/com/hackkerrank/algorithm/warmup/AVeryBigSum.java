package com.hackkerrank.algorithm.warmup;

import java.io.*;
import java.util.*;

public class AVeryBigSum {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int numsNo = scan.nextInt();
		long result = 0;
		for (int i = 0; i < numsNo; i++) {
			result += scan.nextLong();
		}
		System.out.print(result);
	}
}
