package com.hackkerrank.algorithm.warmup;

import java.util.Scanner;

public class StairCase {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		for (int i = 1; i <= num; i++) {
			String starcase = "";
			for (int j = 0; j < num - i; j++) {
				starcase += " ";
			}
			for (int j = 0; j < i; j++) {
				starcase += "#";
			}
			System.out.println(starcase);
		}
	}
}
