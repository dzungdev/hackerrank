package com.hackkerrank.algorithm.warmup;

import java.io.*;
import java.util.*;

public class PlusMinus {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int numsNo = scan.nextInt();
		int[] ints = new int[numsNo];
		int index = 0;
		while (scan.hasNextInt()) {
			ints[index++] = scan.nextInt();
		}
		int posNums = 0;
		int negNums = 0;
		int zeroesNums = 0;
		for (int i : ints) {
			if (i > 0) {
				posNums++;
			} else if (i < 0) {
				negNums++;
			} else {
				zeroesNums++;
			}
		}
		System.out.println((double) posNums / numsNo);
		System.out.println((double) negNums / numsNo);
		System.out.println((double) zeroesNums / numsNo);
	}
}