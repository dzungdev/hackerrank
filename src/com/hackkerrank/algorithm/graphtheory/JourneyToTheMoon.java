package com.hackkerrank.algorithm.graphtheory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class JourneyToTheMoon {

  /**
   * 10 7 
   * 0 2 
   * 1 8 
   * 1 4 
   * 2 8 
   * 2 6 
   * 3 5 
   * 6 9
   * 
   * @param args
   * @throws InterruptedException
   */

  public static void main(String[] args) throws InterruptedException {
    Scanner scan = new Scanner(System.in);
    int n, p;
    n = scan.nextInt();
    p = scan.nextInt();

    Graph g = new Graph(n);
    for (int i = 0; i < p; i++) {
      g.addEdge(scan.nextInt(), scan.nextInt());
    }
    g.solve();
    // System.out.println(g.solve());
    scan.close();
  }
  
  static class Graph {

    int                           v;
    boolean[]                     visited;
    HashMap<Integer, ArrayList<Integer>> graph = new HashMap<>();

    Graph(int v) {
      this.v = v;
      this.visited = new boolean[v];
      for (int i = 0; i < v; i++) {
        graph.put(i, new ArrayList<>());
      }
    }

    public void addEdge(int v, int w) {
      graph.get(v).add(w);
      graph.get(w).add(v);
    }

    public int dfs(int v) {
      int count = 1;
      visited[v] = true;
      for (Integer i : graph.get(v)) {
        if (!visited[i]) {
          count += dfs(i);
        }
      }
      return count;
    }

    public ArrayList<Integer> dfsTraversal() {
      ArrayList<Integer> countrySizes = new ArrayList<>();
      System.out.println("visited length: " + visited.length);
      for (int i = 0; i < visited.length; i++) {
        if (!visited[i]) {
          countrySizes.add(dfs(i));
        }
      }
      return countrySizes;
    }

    public long solve() {
      ArrayList<Integer> countrySizes = dfsTraversal();

      countrySizes.stream().forEach(e -> {
        System.out.print(e + " ");
      });

      long sum = 0, result = 0;
      for (int size : countrySizes) {
        result += sum * size;
        sum += size;
      }
      return result;
    }

  }

}

