package com.hackkerrank.algorithm.graphtheory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class RoadsAndLibraries {
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int q = scan.nextInt();
    for (int a0 = 0; a0 < q; a0++) {
      int n = scan.nextInt();
      int m = scan.nextInt();
      int libraryCost = scan.nextInt();
      int roadCost = scan.nextInt();
      
      if (libraryCost < roadCost) {
        System.out.println(n * libraryCost);
      } else {
        long cost = 0;
        Graph graph = new Graph(n);
        for(int i = 0; i < m;i++) {
          graph.addEdge(scan.nextInt(), scan.nextInt());
        }
        
        System.out.println(graph.getCost(libraryCost, roadCost));
        
//        for(Integer city: graph.keySet()) {
//          int nodeNum = graph.dfs(city);
//          if (nodeNum > 1) {
//            cost += (nodeNum - 1) * roadCost + libraryCost;
//          }
//        }
//        System.out.println(cost);
      }
    }  
  }

  static class Graph {

    int                  v;
    boolean[]            visited;
    Map<Integer, List<Integer>> graph = new HashMap<>();

    Graph(int v) {
      this.v = v;
      this.visited = new boolean[v+1];

      for (int i = 1; i <= v; i++) {
        graph.put(i, new ArrayList<>());
      }
    }

    public void addEdge(int v, int w) {
      graph.get(v).add(w);
      graph.get(w).add(v);
    }

    public int dfs(int v) {
      int count = 1;
      visited[v] = true;
//      System.out.println("v is: " + v + ", list: " + graph.get(v));
      for (Integer neighbour : graph.get(v)) {
        if (!visited[neighbour]) {
          count += dfs(neighbour);
        }
      }
      return count;
    }
    
    public ArrayList<Integer> dfsTraversal() {
      ArrayList<Integer> citySizes = new ArrayList<>();
      for (int i = 1; i < visited.length; i++) {
        if (!visited[i]) {
          citySizes.add(dfs(i));
        }
      }
      return citySizes;
    }
    
    public long getCost(int libraryCost, int roadCost) {
      List<Integer> citySizes = dfsTraversal();
      long cost = 0;
      for(Integer size: citySizes) {
        cost += (size -1) * roadCost + libraryCost;
      }
      return cost;
    }
  }

//  public static void main(String[] args) {
//    Scanner scan = new Scanner(System.in);
//    int q = scan.nextInt();
//    for (int a0 = 0; a0 < q; a0++) {
//      HashMap<Integer, ArrayList<Integer>> cityMap = new HashMap<>();
//      int n = scan.nextInt();
//      int m = scan.nextInt();
//      int libraryCost = scan.nextInt();
//      int roadCost = scan.nextInt();
//      for (int i = 1; i <= n; i++) {
//        ArrayList<Integer> list = new ArrayList<Integer>();
//        list.add(i);
//        cityMap.put(i, list);
//      }
//
//      for (int a1 = 0; a1 < m; a1++) {
//        int x = scan.nextInt();
//        int y = scan.nextInt();
//
//        ArrayList<Integer> list1 = cityMap.get(x);
//        ArrayList<Integer> list2 = cityMap.get(y);
//        if (list1 != list2) {
//          list1.addAll(list2);
//          list2.forEach(i -> cityMap.put(i, list1));
//        }
//      }
//      if (libraryCost < roadCost)
//        System.out.println((long) n * libraryCost);
//      else {
//        long cost = 0;
//
//        // for(Integer key: cityMap.keySet()) {
//        // System.out.println("key is: " + key);
//        // ArrayList<Integer> list = cityMap.get(key);
//        // list.forEach(e -> {System.out.print(e + " ");});
//        // System.out.println("");
//        // }
//
//        for (ArrayList<Integer> list : cityMap.values()) {
//          // list.forEach(e -> {System.out.print(e + " ");});
//
//          int size = list.size();
//          if (size > 0) {
//            cost += libraryCost;
//            cost += (size - 1) * roadCost;
//            list.removeIf(i -> true);
//          }
//        }
//        System.out.println(cost);
//      }
//
//    }
//    scan.close();
//  }

}
