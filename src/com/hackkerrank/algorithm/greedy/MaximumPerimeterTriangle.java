package com.hackkerrank.algorithm.greedy;

import java.util.Arrays;
import java.util.Scanner;

public class MaximumPerimeterTriangle {
  
  /**
   * 44
   * 1 28657 55 1346269 121393 4181 17711 2584 233 3 701408733 63245986 21 75025 13 3524578 34 10946 5 196418 46368 8 102334155 987 9227465 1597 610 317811 267914296 1 144 2178309 165580141 514229 832040 377 6765 2 89 24157817 433494437 5702887 39088169 14930352
   * @param arr
   * @param leftArr
   * @param rightArr
   */
  private static void merge(Integer[] arr, Integer[] leftArr, Integer[] rightArr) {
    int i = 0, j = 0, k = 0;
    while (i < leftArr.length && j < rightArr.length) {
      if (leftArr[i] <= rightArr[j]) {
        arr[k] = leftArr[i];
        i++;
      } else {
        arr[k] = rightArr[j];
        j++;
      }
      k++;
    }
    while (i < leftArr.length) {
      arr[k] = leftArr[i];
      i++;k++;
    }
    while (j < rightArr.length) {
      arr[k] = rightArr[j];
      j++;k++;
    }
  }
  
  private static void mergeSort(Integer[] arr) {
    if (arr.length < 2) return;
    int mid = arr.length / 2;
    Integer[] leftArr = new Integer[mid];
    Integer[] rightArr = new Integer[arr.length - mid];
    for(int i = 0; i < mid; i++) {
      leftArr[i] = arr[i];
    }
    for(int i = mid; i < arr.length; i++) {
      rightArr[i - mid] = arr[i];
    }
    mergeSort(leftArr);
    mergeSort(rightArr);
    merge(arr, leftArr, rightArr);
  }
  
  public static void printMaximum(Integer[] arr) {
    mergeSort(arr);
    
    for(int i = 0; i < arr.length;i++) {
      System.out.print(arr[i] + " ");
    }
    System.out.println("");
    
    for(int i = arr.length - 3; i >= 0; i--) {
      if (arr[i] + arr[i+1] > arr[i+2]) {
        System.out.print(arr[i] + " " + arr[i+1] + " " + arr[i+2]);
        return;
      }
    }
    System.out.print(-1);
    
//    Arrays.sort(arr, Collections.reverseOrder());
//    for (int i = 0; i < arr.length - 2; i++) {
//        if (arr[i] <= arr[i+1] + arr[i+2]) {
//            System.out.println(arr[i+2] + " " + arr[i+1] + " " + arr[i]);
//            return;
//        }
//    }
//    System.out.println(-1);
  }
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int n = scan.nextInt();
    Integer[] arr = new Integer[n];
    for(int i = 0; i < n; i++) {
      arr[i] = scan.nextInt();
    }
    scan.close();
    printMaximum(arr);
    
  }
  
}
