package com.hackkerrank.algorithm.greedy;

import java.util.Scanner;

public class GreedyFlorist {
  
  private static void merge(int[] leftArr, int[] rightArr, int[] arr) {
    int i = 0, j = 0, k = 0;
    while (i < leftArr.length && j < rightArr.length) {
      if (leftArr[i] > rightArr[j]) {
        arr[k] = leftArr[i];
        i++;
      } else {
        arr[k] = rightArr[j];
        j++;
      }
      k++;
    }
    while (i < leftArr.length) {
      arr[k] = leftArr[i];
      i++;
      k++;
    }
    while (j < rightArr.length) {
      arr[k] = rightArr[j];
      j++;
      k++;
    }
  }

  private static void mergeSort(int[] arr) {
    if (arr.length < 2) return;// if the arr is 1 element, then sorted and return
    int mid = arr.length / 2;
    int[] leftArr = new int[mid];
    int[] rightArr = new int[arr.length - mid];
    for (int i = 0; i < mid; i++) {
      leftArr[i] = arr[i];
    }
    for (int i = mid; i < arr.length; i++) {
      rightArr[i - mid] = arr[i];
    }
    mergeSort(leftArr);
    mergeSort(rightArr);
    merge(leftArr, rightArr, arr);
  }
  
  static int maxiumumToys(int[] prices, int k) {
    mergeSort(prices);
    if (prices[0] > k) return 0;
    int maximumToys = 0;
    for (int i = 0; i < prices.length;i++) {
      if (prices[i] <= k) {
        maximumToys++;
        k = k - prices[i];
      } else {
        break;
      }
    }
    
    return maximumToys;
  }
  
  static int getMinimumCost(int n, int k, int[] c) {
    //sorting to have DESC ordering
    mergeSort(c);
    
    int minimumCost = 0;
    for (int i = 0; i < n;i++) {
      minimumCost += ((i / k) +1) * c[i];
    }
    return minimumCost;
  }
  

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int k = in.nextInt();
    int[] c = new int[n];
    for (int c_i = 0; c_i < n; c_i++) {
      c[c_i] = in.nextInt();
    }
    in.close();
    int minimumCost = getMinimumCost(n, k, c);
    System.out.println(minimumCost);
  }
}
