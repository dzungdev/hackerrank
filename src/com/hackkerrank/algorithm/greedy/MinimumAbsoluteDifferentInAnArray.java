package com.hackkerrank.algorithm.greedy;

import java.util.Scanner;

public class MinimumAbsoluteDifferentInAnArray {

  private static void merge(int[] leftArr, int[] rightArr, int[] arr) {
    int i = 0, j = 0, k = 0;
    while (i < leftArr.length && j < rightArr.length) {
      if (leftArr[i] <= rightArr[j]) {
        arr[k] = leftArr[i];
        i++;
      } else {
        arr[k] = rightArr[j];
        j++;
      }
      k++;
    }
    while (i < leftArr.length) {
      arr[k] = leftArr[i];
      i++;
      k++;
    }
    while (j < rightArr.length) {
      arr[k] = rightArr[j];
      j++;
      k++;
    }
  }

  private static void mergeSort(int[] arr) {
    if (arr.length < 2) return;// if the arr is 1 element, then sorted and return
    int mid = arr.length / 2;
    int[] leftArr = new int[mid];
    int[] rightArr = new int[arr.length - mid];
    for (int i = 0; i < mid; i++) {
      leftArr[i] = arr[i];
    }
    for (int i = mid; i < arr.length; i++) {
      rightArr[i - mid] = arr[i];
    }
    mergeSort(leftArr);
    mergeSort(rightArr);
    merge(leftArr, rightArr, arr);
  }

  static int minimumAbsoluteDifference(int n, int[] arr) {
    if (arr.length == 2) return Math.abs(arr[0] - arr[1]);
    mergeSort(arr);
    int min = arr[1] - arr[0];
    for (int i = 1; i < arr.length-1;i++) {
      if (min > arr[i+1] - arr[i]) min = arr[i+1] - arr[i];
    }
    return min;
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int[] arr = new int[n];
    for (int arr_i = 0; arr_i < n; arr_i++) {
      arr[arr_i] = in.nextInt();
    }
    int result = minimumAbsoluteDifference(n, arr);
    System.out.println(result);
    in.close();
  }

}
