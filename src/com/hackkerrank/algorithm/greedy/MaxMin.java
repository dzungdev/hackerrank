package com.hackkerrank.algorithm.greedy;

import java.util.Scanner;

public class MaxMin {
  
  private static void merge(int[] leftArr, int[] rightArr, int[] arr) {
    int i = 0, j = 0, k = 0;
    while (i < leftArr.length && j < rightArr.length) {
      if (leftArr[i] <= rightArr[j]) {
        arr[k] = leftArr[i];
        i++;
      } else {
        arr[k] = rightArr[j];
        j++;
      }
      k++;
    }
    while (i < leftArr.length) {
      arr[k] = leftArr[i];
      i++;
      k++;
    }
    while (j < rightArr.length) {
      arr[k] = rightArr[j];
      j++;
      k++;
    }
  }

  private static void mergeSort(int[] arr) {
    if (arr.length < 2) return;// if the arr is 1 element, then sorted and return
    int mid = arr.length / 2;
    int[] leftArr = new int[mid];
    int[] rightArr = new int[arr.length - mid];
    for (int i = 0; i < mid; i++) {
      leftArr[i] = arr[i];
    }
    for (int i = mid; i < arr.length; i++) {
      rightArr[i - mid] = arr[i];
    }
    mergeSort(leftArr);
    mergeSort(rightArr);
    merge(leftArr, rightArr, arr);
  }
  
  public static void main(String[] args) throws NumberFormatException {

    Scanner scan = new Scanner(System.in); 
    int n = scan.nextInt();
    int k = scan.nextInt();
    int[] list = new int[n];

    for(int i = 0; i < n; i ++)
       list[i] = scan.nextInt();
    scan.close();
    int unfairness = Integer.MAX_VALUE;
    
    /*
     * Write your code here, to process numPackets N, numKids K, and the packets of candies
     * Compute the ideal value for unfairness over here
    */
    mergeSort(list);
    if (n == k) {
      unfairness = list[n-1] - list[0];
    } else {//assumption is k <= n
      for(int i =0; i <= n-k;i++) {
        if (unfairness > list[i+k-1] - list[i]) unfairness = list[i+k-1] - list[i];
      } 
    }
    System.out.println(unfairness);
 }
  
}
