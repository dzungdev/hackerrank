package com.hackkerrank.algorithm.greedy;

import java.util.Arrays;
import java.util.Scanner;

public class GridChallenge {
  
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int t = in.nextInt();
//    System.out.println("t is: " + t);
    for(int k = 0; k < t; k++) {
      int n = in.nextInt();
//      System.out.println("n is: " + n);
      char[][] grid = new char[n][n];
      for (int i = 0; i < n; i++) {
        String[] letters = in.next().split("");
        for (int j = 0; j < n; j++) {
          grid[i][j] = letters[j].charAt(0);
        }
        Arrays.sort(grid[i]);
      }
      
//      for (int i = 0; i < n; i++) {
//        for (int j = 0; j < n;j++) {
//          System.out.print(grid[i][j] + " ");
//        }
//        System.out.println("");
//       
//      }
      
      String answer = "YES";
      out:
      for(int i = 0; i < n - 1; i++) {
        for (int j = 0; j< n;j++) {
          if ((int)grid[i][j] > (int) grid[i+1][j]) {
            answer = "NO";
            break out;
          }
        }
      }
      System.out.println(answer);
    }
    in.close();
  }
  
}
