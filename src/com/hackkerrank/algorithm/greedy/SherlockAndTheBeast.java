package com.hackkerrank.algorithm.greedy;

import java.util.Scanner;

public class SherlockAndTheBeast {

  // we use n = 5 * x + 3 * y (x,y are integer)
  static void printLargetDecentNumber(int n) {
    StringBuilder sb = new StringBuilder();
    if (n % 3 == 0 && n != 0) {
      for (int i = 0; i < n; i++) {
        sb.append("5");
      }
    } else if (n % 3 == 1 && n >= 10) {
      for (int i = 0; i < n - 10; i++) {
        sb.append("5");
      }
      // 10 * sb("3")
      sb.append("3333333333");
    } else if (n % 3 == 2 && n >= 5) {
      for (int i = 0; i < n - 5; i++) {
        sb.append("5");
      }
      sb.append("33333");
    } else {
      sb.append("-1");
    }
    
    System.out.println(sb.toString());
  }

  

  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int t = scan.nextInt();

    for (int i = 0; i < t; i++) {
      int n = scan.nextInt();
      printLargetDecentNumber(n);
    }
    scan.close();
  }

}
