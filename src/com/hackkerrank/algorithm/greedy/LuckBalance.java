package com.hackkerrank.algorithm.greedy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LuckBalance {
  
  public static long getTotalMaximumLuck(int k, List<Integer> importantContests, List<Integer> notImportantContests) {
    long total = 0;
    importantContests.sort((l1, l2) -> l2.compareTo(l1));
    
//    importantContests.forEach(e -> {System.out.print(e + " ");});
//    System.out.println("");
    
    if (k <= importantContests.size()) {
      for (int i = 0; i < k;i++) {
        total += importantContests.get(i);
      }
      for (int i = k; i < importantContests.size();i++) {
        total -= importantContests.get(i);
      }
    } else {
      for (int i = 0; i < importantContests.size();i++) {
        total += importantContests.get(i);
      }
    }
    for (int i = 0; i < notImportantContests.size(); i++) {
      total+= notImportantContests.get(i);
    }
    return total;
  }
  
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    
    int t = in.nextInt();
    int k = in.nextInt();
    List<Integer> importantContests = new ArrayList<>();
    List<Integer> notImportantContests = new ArrayList<>();
    
    for (int i = 0; i < t; i++) {
      int luck = in.nextInt();
      int isImportant = in.nextInt();
      if (isImportant == 1) {
        importantContests.add(luck);
      } else {
        notImportantContests.add(luck);
      }
    }
    in.close();
    System.out.print(getTotalMaximumLuck(k, importantContests, notImportantContests));
  }
  
}
