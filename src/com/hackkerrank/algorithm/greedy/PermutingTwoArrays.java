package com.hackkerrank.algorithm.greedy;

import java.util.Arrays;
import java.util.Scanner;

public class PermutingTwoArrays {
  
  static void answer(Integer[] a, Integer[] b, int k) {
    Arrays.sort(a);
    Arrays.sort(b, (b1,b2) -> {return b2 - b1;});
    
    String answer = "YES";
    for(int i =0; i < a.length;i++) {
      if(a[i] + b[i] < k) {
        answer = "NO";
        break;
      }
    }
    System.out.println(answer);
  }
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int t = scan.nextInt();
    for (int i = 0; i < t; i++) {
      int n = scan.nextInt();
      int k = scan.nextInt();
      Integer[] a = new Integer[n];
      Integer[] b = new Integer[n];
      for (int j = 0; j < n;j++) {
        a[j] = scan.nextInt();
      }
      for (int j = 0; j < n;j++) {
        b[j] = scan.nextInt();
      }
      
      answer(a,b,k);
    }
    
  }
  
}
