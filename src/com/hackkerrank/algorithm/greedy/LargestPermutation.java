package com.hackkerrank.algorithm.greedy;

import java.util.Scanner;

public class LargestPermutation {
  
  private static void swap(int[] arr, int source, int dest) {
    int temp = arr[source];
    arr[source] = arr[dest];
    arr[dest] = temp;
  }
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int n = scan.nextInt();
    int k = scan.nextInt();
    
    int[] arr = new int[n];
    int[] indexArr = new int[n+1];
    for (int i = 0; i < n; i++) {
      arr[i] = scan.nextInt();
      indexArr[arr[i]] = i;
    }
    scan.close();
    
    for(int i = 0; i < n && k > 0;i++) {
      if (arr[i] == n - i) {
        continue;
      }

      //swap it in original arr
      swap(arr, i, indexArr[n-i]);
      //after swap, need to update value of index array
      swap(indexArr, arr[indexArr[n-i]], n-i);
      k--;
    }
    for(int i = 0; i < n; i++) {
      System.out.print(arr[i] + " ");
    }
  }
  
}
