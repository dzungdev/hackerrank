package com.hackkerrank.algorithm.greedy;

import java.util.AbstractMap.SimpleEntry;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class JimAndTheOrders {

  /**
   * test case 7
   * 
   * 9 
   * 3 3 
   * 1 2 
   * 2 1 
   * 1 5 
   * 5 1 
   * 4 1 
   * 1 4 
   * 2 3 
   * 3 2
   * 
   * answer is: 2 3 6 7 8 9 1 4 5
   * 
   * @param args
   */
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int n = scan.nextInt();
    /**
     * the map will have structure: index, <time, unit> for example: 5 8 1 4 2 5 6
     * then Map will be <0, <8,1>>; <1,<4,2>>; <2,<5,6>>
     */

    // if (e1.getKey() + e1.getValue() == e2.getKey() + e2.getValue()) {
    // return e1.getKey().compareTo(e2.getKey());
    // } else {
    // return e1.getKey() + e1.getValue() - e2.getKey() - e2.getValue();
    // }
    Map<Integer, SimpleEntry<Integer, Integer>> randomMap = new HashMap<>();
    for (int i = 1; i <= n; i++) {
      SimpleEntry entry = new SimpleEntry<>(scan.nextInt(), scan.nextInt());
      randomMap.put(i, entry);
    }
    scan.close();

    // sorting the Map first
    Map<Integer, SimpleEntry<Integer, Integer>> sortedMap = randomMap.entrySet().stream()
        .sorted(new Comparator<Map.Entry<Integer, SimpleEntry<Integer, Integer>>>() {
          @Override
          public int compare(Map.Entry<Integer, SimpleEntry<Integer, Integer>> o1,
              Map.Entry<Integer, SimpleEntry<Integer, Integer>> o2) {
            SimpleEntry<Integer, Integer> e1 = o1.getValue();
            SimpleEntry<Integer, Integer> e2 = o2.getValue();
            if (e1.getKey() + e1.getValue() == e2.getKey() + e2.getValue()) {
              return o1.getKey().compareTo(o2.getKey());
            } else {
              return e1.getKey() + e1.getValue() - e2.getKey() - e2.getValue();
            }
          }
        }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
            LinkedHashMap::new));

    for (Integer index : sortedMap.keySet()) {
      System.out.print(index + " ");
    }

  }

}
