package com.hackkerrank.algorithm.greedy;

import java.util.Scanner;

public class BeautifulPairs {
  
    public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      int n = scan.nextInt();
      //using technique like counting sort
      int[] bucketA = new int[1001];//max N is 10 pow 3
      for (int i = 0; i < n; i++) {
        bucketA[scan.nextInt()]++;
      }
      
      int beautifulPairs = 0;
      for (int i = 0; i < n; i++) {
        int num = scan.nextInt();
        if (bucketA[num] > 0) {
          bucketA[num]--;
          beautifulPairs++;
        }
      }
      scan.close();
      
      if (beautifulPairs == n) {
        beautifulPairs--;
      } else {
        beautifulPairs++;
      }
      System.out.println(beautifulPairs);
    }
  
}
