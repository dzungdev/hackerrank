package com.hackkerrank.algorithm.string;

import java.util.Scanner;

public class SuperReducedString {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String str = scan.nextLine();
		char[] chars = str.toCharArray();
		for (int i = 1; i < chars.length; i++) {
			if (chars[i] == chars[i-1]) {
				str = str.substring(0, i - 1) + str.substring(i+1, chars.length);
				chars = str.toCharArray();
				i = 0;//we put 0 because after this i will be 1, we want it start from 1
			}
		}
		
		if (str == null || str.isEmpty()) {
			str = "Empty String";
		}
		
		System.out.print(str);
		
		scan.close();
		
	}
}