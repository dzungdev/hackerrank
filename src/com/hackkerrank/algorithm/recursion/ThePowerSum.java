package com.hackkerrank.algorithm.recursion;

import java.util.Arrays;
import java.util.Scanner;

import javax.xml.transform.stream.StreamSource;

/**
 * This is Coin Change problem
 * 
 * @author alex
 *
 */
public class ThePowerSum {

  private static int count(int x, int index, int[] arr) {
//    System.out.print("arr is: ");
//    Arrays.stream(arr).forEach(e -> {System.out.print(e + " ");});
//    System.out.println("");
    
    int remaining = x - arr[index];
    
//    System.out.println("index is " + index + ", x is " + x + " ,arr[index] " + arr[index] + " ,remaining is " + remaining );
    if (remaining == 0) {
      return 1;
    } 
    if (remaining < 0) return 0;
    
    int total = 0;
    for (int i = index + 1; i < arr.length; i++) {
//      System.out.println("i is " + i + " x - arr[index] is " + (remaining));
      total += count(remaining, i, arr);
//      System.out.println("===end of deep " + index + " recursion === \n");
    }
    return total;
  }
  
  public static void main(String[] args) {
    
    Scanner in = new Scanner(System.in);
    int x = in.nextInt();
    
//    System.out.println("first x: " + x);
    
    int n = in.nextInt();
    in.close();
    int max = (int)(Math.pow(x, 1.0/n));
    
//    System.out.println("max is: " + max);
    
    int[] arr = new int[max + 1];//because there is 0^2 = 0
    for (int i = 0; i <= max; i++) {
      arr[i] = (int) Math.pow(i, n);
    }
    
//    Arrays.stream(arr).forEach(e -> System.out.print(e + " "));
//    System.out.println("");
    
    System.out.print(count(x, 0, arr));
  }

}
