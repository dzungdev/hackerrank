package com.hackkerrank.algorithm.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;


public class EvenTree {
	
	private static class Node {
		int nodeCount = 1;
		int data;
		
		Set<Node> children = new HashSet<Node>();
		
		Node(int data) {this.data = data;}
	}
	
	private static int countNodes(Node root) {
		for (Node child: root.children) {
			root.nodeCount += countNodes(child);
		}
		return root.nodeCount;
	}
	
	public static int countEvenNode(Node root) {
		//calculate the number of subnode for all nodes in tree
		countNodes(root);
		
		int counter = 0;
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		
		while (queue.size() >0) {
			Node current = queue.remove();
			for (Node child: current.children) {
				if (child.nodeCount % 2 == 0) {// child is even tree
					counter++;
				}
				queue.add(child);//add to loop through tree
			}
		}
		return counter;
		
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int m = scan.nextInt();
		
		//we will use Map to store tree like data is key, Node is value
		Map<Integer, Node> nodes = new HashMap<>();
		
		//root
		nodes.put(1, new Node(1));
		
		for (int i = 0; i < m; i++) {
			int data1 = scan.nextInt();
			int data2 = scan.nextInt();
			Node parent = null;
			Node newNode = null;
			if (nodes.containsKey(data1)) {
				parent = nodes.get(data1);
				newNode = new Node(data2);
			} else {
				parent = nodes.get(data2);
				newNode = new Node(data1);
			}
			parent.children.add(newNode);
			nodes.put(newNode.data, newNode);
		}
		
		System.out.print(countEvenNode(nodes.get(1)));
		
	}
}
