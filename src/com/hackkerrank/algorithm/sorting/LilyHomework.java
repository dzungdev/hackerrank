package com.hackkerrank.algorithm.sorting;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class LilyHomework {
	
	private static void swap(int[] arr, int source, int dest) {
		int temp = arr[source];
		arr[source] = arr[dest];
		arr[dest] = temp;
	}

	public static int minSwaps(int[] arr) {
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < arr.length; i++) {
			map.put(arr[i], i);
		}
		
		int totalSwap = 0;
		int[] sorted_arr = new int[arr.length];
		for (int i = 0; i < arr.length; i++) {
			sorted_arr[i] = arr[i];
		}
		Arrays.sort(sorted_arr);
		
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != sorted_arr[i]) {
				totalSwap++;
				int index_sorted_val_in_not_sorted = map.get(sorted_arr[i]);
				map.put(arr[i], index_sorted_val_in_not_sorted);
				swap(arr, i, index_sorted_val_in_not_sorted);
			}
		}
		return totalSwap;
	}

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = in.nextInt();
		}

		int[] reversedArr = new int[n];
		for (int j = n - 1; j >= 0; j--) {
			reversedArr[n - 1 - j] = arr[j];
		}

		int asc = minSwaps(arr);
		int desc = minSwaps(reversedArr);

		System.out.println(asc < desc ? asc : desc);
	}

}
