package com.hackkerrank.algorithm.sorting;

import java.util.ArrayList;
import java.util.List;

public class MergingOverlapingIntervals {
  
  /**
   * 1) Sort all intervals in decreasing order of start time.
   * 2) Traverse sorted intervals starting from first interval, 
         do following for every interval.
      a) If current interval is not first interval and it 
         overlaps with previous interval, then merge it with
         previous interval. Keep doing it while the interval
         overlaps with the previous one.         
      b) Else add current interval to output list of intervals. 
   */
  private static void merge(int[][] leftArr, int[][] rightArr, int[][] arr) {
    int i = 0, j = 0, k = 0;
    while (i < leftArr.length && j < rightArr.length) {
      if (leftArr[i][0] > rightArr[j][0]) {
        arr[k] = leftArr[i];
        i++;
      } else {
        arr[k] = rightArr[j];
        j++;
      }
      k++;
    }
    while (i < leftArr.length) {
      arr[k] = leftArr[i];
      i++;k++;
    }
    while (j < rightArr.length) {
      arr[k] = rightArr[j];
      j++;k++;
    }
  }
  
  private static void mergeSort(int[][] arr) {
    if (arr.length < 2) return;
    int mid = arr.length / 2;
    int[][] leftArr = new int[mid][];
    int[][] rightArr = new int[arr.length - mid][];
    for (int i = 0; i < mid;i++) {
      leftArr[i] = arr[i];
    }
    for(int i = mid; i < arr.length; i++) {
      rightArr[i-mid] = arr[i];
    }
    mergeSort(leftArr);
    mergeSort(rightArr);
    merge(leftArr, rightArr, arr);
  }
  
  
  public static void main(String[] args) {
    int[][] arr = {{1,3}, {2,6}, {8,10}, {15,18}};
    mergeSort(arr);
    
    /**
     * 2) Traverse sorted intervals starting from first interval, 
         do following for every interval.
      a) If current interval is not first interval and it 
         overlaps with previous interval, then merge it with
         previous interval. Keep doing it while the interval
         overlaps with the previous one.         
      b) Else add current interval to output list of intervals. 
     */    
    List<List<Integer>> intervals = new ArrayList<List<Integer>>();
    List<Integer> firstInterval = new ArrayList<>();
    firstInterval.add(arr[0][0]);
    firstInterval.add(arr[0][1]);
    intervals.add(firstInterval);
    
    for (int i = 0; i < arr.length; i++) {
      System.out.print(arr[i][0] + " " + arr[i][1] + "; ");
    }
    System.out.println("");
    
    for (int i = 1; i < arr.length; i++) {
      int start = arr[i][0];
      int end = arr[i][1];
      
      System.out.println("i is " + i + ",start="+ start + ",end=" + end);
      
      List<Integer> lastInterval = intervals.get(intervals.size() - 1);
      int prevStart = lastInterval.get(0);
      int prevEnd = lastInterval.get(1);
      System.out.println("i is " + i + ",prevStart="+ prevStart + ",prevEnd=" + prevEnd);
      
      if (end < prevStart || prevStart > prevEnd) {//not overlap
        List<Integer> newInterval = new ArrayList<>();
        newInterval.add(start);
        newInterval.add(end);
        intervals.add(newInterval);
      } else {//overlap
        if (start < prevStart) lastInterval.set(0, start);
        if(end > prevEnd) lastInterval.set(1, end);
      }
    }
    
    for(List<Integer> interval: intervals) {
      System.out.println(interval.get(0) + " " + interval.get(1));
    }
  }
}
