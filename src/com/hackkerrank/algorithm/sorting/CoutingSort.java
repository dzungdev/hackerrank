package com.hackkerrank.algorithm.sorting;

public class CoutingSort {
  
  static void sort(int[] arr) {
    int[] outputs = new int[arr.length];
    // the biggest value of the element
    int[] counts = new int[8];
    for(int i = 0; i < 8; i++) {
      counts[i] = 0;
    }
    
    //count the element of original list displays how many times
    for(int i = 0; i < arr.length; i++) {
      ++counts[arr[i]];
    }
    
    //re-calculate to make count[i] contains actualy position of arr[i] in output
    for(int i = 1; i < counts.length; i++) {
      counts[i] += counts[i-1];
    }
    
    for (int i = 0; i < arr.length;i++) {
      outputs[counts[arr[i]] - 1] = arr[i];
      --counts[arr[i]];
    }
    
    //copy outputs to arr 
    for(int i = 0; i < arr.length; i++) {
      arr[i] = outputs[i];
    }
  }
  
  public static void main(String[] args) {
    int[] arr = {1,4,1,2,7,5,2};
    sort(arr);
    for(int i: arr) {
      System.out.print(i + " ");
    }
  }
  
}
