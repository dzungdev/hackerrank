package com.hackkerrank.algorithm.sorting;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

public class BigSorting {
	
	/**
	 * 1. Select a pivot by middle of left and right
	 * 2. create 2 index cursor to run from left -> right and right -> left
	 * 3. while i <= j
	 * 	  i: run from left to right to find element greater than pivotVal
	 *    j: run from right to left to find element smaller than pivotVal
	 *    once we can find that 2 element, need to check whether arr[i] in right side
	 *    of arr[j] or not, if not, then swap and make i,j move one step
	 *    move one step means: i++ (run from left to right), j-- (run from right to left)
	 * 4. when i > j, mean there are 2 partition
	 *    i: first element greater than pivotVal in right array
	 *    j: actually it is pivotVal element
	 *    apply recursively sort for this 2 array.
	 *    Base case will be left > j and right < i
	 */
	public static void quicksortOptimized(String[] arr, int left, int right) {
		String pivotVal = arr[(left + right) / 2];
		int i = left;
		int j = right; // j is head of Greater array
		// loop from start to end
		while (i <= j) {
			while (isBigger(pivotVal, arr[i]))
				i++;// advance current position
			while (isBigger(arr[j], pivotVal))
				j--;
			// after 2 while above, then data[i] >= pivotVal and data[j] <= pivotVal
			// then if the i <= j but data[i] > data[j] (by using pivotVal to compare)
			// so we need to swap
			if (i <= j) {
				swap(arr, i, j);
				i++;
				j--;
			}
		}
		// after running this, we have 2 partition
		// i: will be the first position of element greater than pivotVal in right side (G array)
		// j: is the position of pivotVal and left -> j: left side (smaller than pivotVal)
		// i -> right: right side (greater than pivotVal)
		if (left < j) {
			quicksortOptimized(arr, left, j);
		}
		if (i < right) {
			quicksortOptimized(arr, i, right);
		}
	}
   
   private static boolean isBigger(String str1, String str2) {
  	 if (str1.length() < str2.length()) {
  		 return false;
  	 } else if (str1.length() == str2.length()) {
  		 return str1.compareTo(str2) > 0;
  	 } else  {
  		 return true;
  	 }
   }
   
   private static void swap(String[] arr, int source, int dest) {
 		String temp = arr[source];
 		arr[source] = arr[dest];
 		arr[dest] = temp;
 	}

   public static void main(String[] args) {
       Scanner in = new Scanner(System.in);
       int n = in.nextInt();
       
       String[] unsorted = new String[n];
       for(int unsorted_i=0; unsorted_i < n; unsorted_i++){
           unsorted[unsorted_i] = in.next();
       }
       // your code goes here
       quicksortOptimized(unsorted, 0, unsorted.length - 1);
       Arrays.stream(unsorted).forEach(System.out::println);
   }
}
