package com.hackkerrank.algorithm.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class QuickSortOne {
	
	private static int[] partition(int[] ar) {
		List<Integer> equal = new ArrayList<>();
		List<Integer> left = new ArrayList<>();
		List<Integer> right = new ArrayList<>();
		
		int pVal = ar[0];
		equal.add(pVal);
		for (int i = 1; i < ar.length; i++) {
			if (ar[i] < pVal) {
				left.add(ar[i]);
			} else if (ar[i] > pVal) {
				right.add(ar[i]);
			} else {
				equal.add(ar[i]);
			}
		}
		
		ar = Stream.concat(Stream.concat(left.stream(), equal.stream()), right.stream()).mapToInt(Integer::intValue).toArray();
		return ar;
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
    int num = scan.nextInt();
    int[] ar = new int[num];
    for (int i = 0; i < num; i++) {
        ar[i] = scan.nextInt();
    }
    
    ar = partition(ar);
    System.out.print(Arrays.stream(ar).mapToObj(String::valueOf).collect(Collectors.joining(" ")));
    
    scan.close();
	}
	
	
}
