package com.hackkerrank.algorithm.sorting;

import java.util.Arrays;
import java.util.Scanner;

public class CountingSortFull {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		int[] originalIntAr = new int[num];
		String[] originalStringAr = new String[num];
		int[] sumCountAr = new int[100];//x <100
		
		for (int i = 0; i < num; i++) {
			int val = scan.nextInt();;
			originalIntAr[i] = val;
			sumCountAr[val]++;//calculate the occurence of element 
			String text = scan.next();
			originalStringAr[i] = i < num/2 ? "-" : text;
		}
		scan.close();
		
		//calculate sumCountar
		for (int i = 1; i < 100; i++) {
			sumCountAr[i] += sumCountAr[i-1];
		}
		
		String[] sorted = new String[num];
		//because there is duplicated so we must have stable sort, so we run from right to left
		for (int i = num - 1; i >= 0; i--) {
			int inputIntVal = originalIntAr[i];
			int position = sumCountAr[inputIntVal];
			
			sorted[position-1] = originalStringAr[i];//reduce 1 because the 20th element is 19th in array
			sumCountAr[inputIntVal]--;//reduce for duplicated
		}
		
		Arrays.stream(sorted).forEach(e -> {System.out.print(e + " ");});
		
	}
	
}
