package com.hackkerrank.algorithm.sorting;

import java.util.Arrays;
import java.util.Scanner;

public class FraudulentActivityNotifications {
  
  private static int freq_length = 9;

  private static void printArr(int[] arr) {
    Arrays.stream(arr).forEach(e -> System.out.print(e + " "));
    System.out.println("");
  }

  public static double getMedian(int freq[], int d) {
    int counts_sum[] = new int[freq_length];
    counts_sum[0] = freq[0];
    for (int i = 1; i < freq_length; i++) {
      counts_sum[i] = counts_sum[i - 1] + freq[i];
    }
    
    System.out.println("counter arr: ");
    printArr(counts_sum);
    
    double median;
    int a = 0;
    int b = 0;
    if (d % 2 == 0) {
      int first = d / 2;
      int second = first + 1;
      System.out.println("first: " + first);
      System.out.println("second: " + first);
      int i = 0;
      for (; i < freq_length; i++) {
        if (first <= counts_sum[i]) {
          System.out.println("===> a is: " + i + ", prefix_sum[i] is: " + counts_sum[i]);
          a = i;
          break;
        }
      }
      for (; i < freq_length; i++) {
        if (second <= counts_sum[i]) {
          System.out.println("===> b is: " + i + ", prefix_sum[i] is: " + counts_sum[i]);
          b = i;
          break;
        }
      }

    } else {
      int first = d / 2 + 1;
      for (int i = 0; i < freq_length; i++) {
        if (first <= counts_sum[i]) {
          System.out.println("===> i is: " + i + ", prefix_sum[i] is: " + counts_sum[i]);
          a = i;
          break;
        }
      }
    }
    median = d % 2 == 0 ? ((double)(a + b) / 2) : a;
    return median;
  }

  static int activityNotifications(int[] expenditure, int d) {
    int total = 0;
    int freq[] = new int[freq_length];
    boolean first_time = true;
    int n = expenditure.length;
    
    
    
    
    int pop_element = 0;
    for (int index = d; index < n; index++) {
        if (first_time) {
            first_time = false;
            for (int i = index - d; i <= index - 1; i++)
                freq[expenditure[i]]++;
        } else {
            freq[pop_element]--;
            freq[expenditure[index - 1]]++;
        }
        
        System.out.println("freq arr: ");
        printArr(freq);
        
        double median = getMedian(freq, d);
        if (expenditure[index] >= 2 * median) total++;
        pop_element = expenditure[index - d];
        
        System.out.println("===end for index " + index + " =====");
    }
    return total;
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int d = in.nextInt();
    int[] expenditure = new int[n];
    for (int expenditure_i = 0; expenditure_i < n; expenditure_i++) {
      expenditure[expenditure_i] = in.nextInt();
    }
    long result = activityNotifications(expenditure, d);
    System.out.println(result);
    in.close();
  }
}
