package com.hackkerrank.algorithm.sorting;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuickSortTwo {
	 
 private static int partition(int[] ar, int start, int end) {
     int pVal = ar[start];
     List<Integer> leftList = new ArrayList<>();
     List<Integer> rightList = new ArrayList<>();
     for(int i = start + 1; i <= end; i++) {
    	 if (ar[i] > pVal) {
    		 rightList.add(ar[i]);
    	 } else {
    		 leftList.add(ar[i]);
    	 }
     }
     
//     int[] leftAr = leftList.stream().mapToInt(Integer::intValue).toArray();
//     int[] rightAr = rightList.stream().mapToInt(Integer::intValue).toArray();
     int pIndex = start + leftList.size();
     
//     System.out.println("pIndex: " + pIndex);
     
//     System.out.print("left list: ");
//     leftList.forEach(e -> System.out.print(e + " " ));
//     System.out.println("");
     
//     System.out.print("rightList list: ");
//     rightList.forEach(e -> System.out.print(e + " " ));
//     System.out.println("");
     
     //recreate ar
     copyListToAr(leftList, ar, start);
     copyListToAr(rightList, ar, pIndex+1);
     ar[pIndex] = pVal;
     
     
//     System.out.println(Arrays.stream(ar).mapToObj(String::valueOf).collect(Collectors.joining(" ")));
//     System.out.println("--------");
     return pIndex;
 }
 
 private static void copyListToAr(List<Integer> list, int[] ar, int start) {
	 for(Integer i: list) {
		 ar[start++] = i;
	 }
 }
 
 private static void quicksort(int[] ar, int start, int end) {
     if (start >= end) {return;}
     int pIndex = partition(ar, start, end);
     quicksort(ar, start, pIndex - 1);
     quicksort(ar, pIndex + 1, end);
     printArray(ar, start, end);
 }
 
 private static void printArray(int[] ar, int start, int end) { 
	 while (start <= end) {
		 System.out.print(ar[start++] + " ");
	 }
	 System.out.print("\n");
 }

 public static void main(String[] args) {
     Scanner scan = new Scanner(System.in);
     int num = scan.nextInt();
     int[] ar = new int[num];
     for (int i = 0; i < num; i++) {
         ar[i] = scan.nextInt();
     }
     quicksort(ar, 0, ar.length - 1);
     scan.close();
 }
	
}
