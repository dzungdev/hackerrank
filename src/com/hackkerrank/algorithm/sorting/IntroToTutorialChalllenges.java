package com.hackkerrank.algorithm.sorting;

import java.util.Scanner;

public class IntroToTutorialChalllenges {

	private static int binarySearch(int[] arr, int searchedVal, int start, int end) throws Exception {
		int range = end - start;
		
		if (range == 0 && arr[start] != searchedVal) {
			throw new Exception("No element in array");
		}
		
		int center = range/2 + start;// get middle of range
		
		if (arr[center] == searchedVal) {
			return  center;
		} else if (arr[center] < searchedVal) {
			return binarySearch(arr, searchedVal, center + 1, end);
		} else {
			return binarySearch(arr, searchedVal, start, center - 1);
		}
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int searchedVal = scan.nextInt();
		int num = scan.nextInt();
		int[] arr = new int[num];
		for (int i = 0; i < num; i++) {
			arr[i] = scan.nextInt();
		}
		try {
			System.out.print(binarySearch(arr, searchedVal, 0, arr.length - 1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
