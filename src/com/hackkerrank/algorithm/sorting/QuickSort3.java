package com.hackkerrank.algorithm.sorting;

import java.util.Arrays;
import java.util.Scanner;

public class QuickSort3 {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		//this array contains from 0 to maxvalue, maxvalue is 99 because x < 100
		int[] ar = new int[100];//because x < 100
		for (int i = 0; i < num; i++) {
			int val = scan.nextInt();
			scan.next();
			ar[val]++;
		}
		
		for (int i = 1; i < 100; i++) {
			ar[i] += ar[i-1];
		}
		
		Arrays.stream(ar).forEach(e -> System.out.print(e + " "));
		
	}
	
}
