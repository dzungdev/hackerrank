package com.hackkerrank.algorithm.sorting;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class InsertionSortOne {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		int[] ar = new int[num];
		for (int i = 0; i < num; i++) {
			ar[i] = scan.nextInt();
		}
		int eVal = ar[ar.length - 1];
		int smallerValPos = -1;//in the special case that all elements greater than the val we need to insert
		for (int i = ar.length - 2; i >= 0; i--) {
			if (ar[i] > eVal) {
				ar[i+1] = ar[i];//shift current bigger val to right
			} else {
				smallerValPos = i;
				break;
			}
			System.out.println(Arrays.stream(ar).mapToObj(String::valueOf).collect(Collectors.joining(" ")));
		}
		// after we have smallerValPos, we just insert the eVal to the right pos of that smallerValPos
		
		ar[smallerValPos+1] = eVal;
		System.out.println(Arrays.stream(ar).mapToObj(String::valueOf).collect(Collectors.joining(" ")));
		
		scan.close();
		
	}
}
