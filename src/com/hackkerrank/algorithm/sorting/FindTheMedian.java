package com.hackkerrank.algorithm.sorting;

import java.util.Scanner;


public class FindTheMedian {
    
    private static void swap(int[] arr, int source, int dest) {
        int temp = arr[source];
        arr[source] = arr[dest];
        arr[dest] = temp;
    }
    
    public static int partition(int[] arr, int start, int end) {
        int pivotVal = arr[end]; // select end is pivot element
        int i = start; //the position of 1st element bigger or equal pivotVal
        for (int j = start; j < end; j++) {
            if (arr[j] < pivotVal) {
                swap(arr, i, j);
                i++;
            }
        }
        //after move all smaller to left, we need to swap pivotVal to position just after last smaller val
        swap(arr, i, end);
        return i;
    }
    
    public static void quicksort(int[] arr, int start, int end) {
        if (start < end) {//base condition
            int pivotIndex = partition(arr, start, end);
            quicksort(arr, start, pivotIndex - 1);
            quicksort(arr, pivotIndex + 1, end);
        }
    }

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for(int i=0; i < n; i++){
            arr[i] = in.nextInt();
        }
        
        quicksort(arr, 0, arr.length - 1);
        System.out.print(arr[(arr.length - 1)/2]);
    }
}