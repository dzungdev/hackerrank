package com.hackkerrank.algorithm.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

import javax.xml.transform.stream.StreamSource;

public class QuickSortEnd {
  
  private static void swap(int[] arr, int source, int dest) {
    int sourceVal = arr[source];
    arr[source] = arr[dest];
    arr[dest] = sourceVal;
  }
  
  private static int partition(int[] arr, int start, int end) {
    //use last element is pivot
    int pivot = arr[end];
    
    int i = start; //Index of of smaller element
    
    for (int j = start; j < end; j++) {
      if (arr[j] < pivot) {
        swap(arr, i, j);
        i++;
      }
    }
    
    //swap the pivot value to position after last smaller element
    swap(arr, i, end);
    return i;
  }
  
  private static void quicksort(int[] arr, int start, int end) {
    if (start < end) {
      
      int pivotIndex = partition(arr, start, end);
      quicksort(arr, start, pivotIndex - 1);
      quicksort(arr, pivotIndex + 1, end);
    }
  }
  
  private static void printClosetPair(int[] arr) {
    quicksort(arr, 0, arr.length - 1);
    int min = arr[1] - arr[0];
    
    
    for(int i = 1; i < arr.length - 1; i++) {
      if (arr[i+1] - arr[i] < min) min = arr[i+1] - arr[i];
    }
    
    for (int i = 0; i< arr.length - 1; i++) {
      if ((arr[i+1] - arr[i]) == min) 
        System.out.print(arr[i] + " " + arr[i+1] + " ");
    }
  }
  
  public static void main(String[] args) {
    
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int[] arr = new int[n];
    for(int i=0; i < n; i++){
        arr[i] = in.nextInt();
    }
    
    printClosetPair(arr);
  }
  
}
