package com.hackkerrank.algorithm.search;

import java.util.Iterator;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class MaximumSubarraySum {
  
//  private static int[] arr = {3,3,9,9,5};
  
  static long maximumSum(long[] arr, long m) {
    long maxModulo=0;
    
    TreeSet<Long> prefix = new TreeSet<Long>();
    long currentSum=0;
    for(int i=0;i<arr.length;i++){
      System.out.println("arr[" + i +  "] is: " + arr[i]);
      System.out.println("currentSum before calculate: " + currentSum);
      currentSum=(currentSum+arr[i]%m)%m;
      
      System.out.println("currentSum after calculate: " + currentSum);
      
      System.out.print("set before tailSet: ");
      for (Long e:prefix) {
        System.out.print(e + " ");
      }
      System.out.println("");
      
      System.out.println("new current sum is: " + currentSum);
      SortedSet<Long> set = prefix.tailSet(currentSum+1);
      System.out.print("tailset is: ");
      for (Long e:set) {
        System.out.print(e + " ");
      }
      System.out.println("");
      
      
      Iterator<Long> itr = set.iterator();
      if(itr.hasNext()){
        Long next = itr.next();
        System.out.println("next is: "+ next);
        maxModulo= Math.max(maxModulo, (currentSum-next+m)%m);
      }
      
      maxModulo=Math.max(maxModulo, currentSum);
      prefix.add(currentSum);
      System.out.println("==========> ");
    }
    
    
    return maxModulo;
}
  
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int q = in.nextInt();
    for(int a0 = 0; a0 < q; a0++){
        int n = in.nextInt();
        long m = in.nextLong();
        long[] a = new long[n];
        for(int a_i = 0; a_i < n; a_i++){
            a[a_i] = in.nextLong();
        }
        long result = maximumSum(a, m);
        System.out.println(result);
    }
    in.close();
    
  }
}
