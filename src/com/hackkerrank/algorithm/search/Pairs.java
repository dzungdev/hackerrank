package com.hackkerrank.algorithm.search;

import java.util.Scanner;

public class Pairs {
  
  private static void swap(int[] arr, int source, int dest) {
    int sourceVal = arr[source];
    arr[source] = arr[dest];
    arr[dest] = sourceVal;
  }
  
  private static int partition(int[] arr, int start, int end) {
    int pVal = arr[end];
    int j = start;
    for(int i = start; i < end;i++) {
      if (arr[i] < pVal) {
        swap(arr, i, j);
        j++;
      }
    }
    swap(arr, end, j);
    return j;
  }
  
  private static void quicksort(int[] arr, int start, int end) {
    if (start < end) {
      int pivotIndex = partition(arr, start, end);
      quicksort(arr, start, pivotIndex-1);
      quicksort(arr, pivotIndex+1, end);
    }
  }
  
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int k = in.nextInt();
    int[] arr = new int[n];
    for (int i = 0; i < n; i++) {
      arr[i] = in.nextInt();
    }
    
    quicksort(arr, 0, arr.length - 1);
    //2 pointers approach
    int i = 0, j = 1, result = 0;
    while (j < arr.length) {
      int diff = arr[j] - arr[i];
      if (diff > k) {
        i++;
      } else if (diff == k) {
        result++;
        j++;
      } else {//diff < k
        j++;
      }
    }
    
    System.out.print(result);
    in.close();
  }
}
