package com.hackkerrank.algorithm.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PasswordCracker {
  private static List<String> visited = new ArrayList<>();

  
  private static boolean crack(String loginAtempt, String[] passwords, List<String> res, int level) {
//    String tab = "";
//    for (int i = 0; i < level - 1; i++) {
//        tab += "\t";
//    }
//    System.out.println(tab + "This is the " + level + " recursive call. Input value: " + loginAtempt);
    
    if (loginAtempt.isEmpty()) return true;
    if (visited.contains(loginAtempt)) return false;
    
    for (String pass: passwords) {
//      System.out.println(tab + "This is the " + level + " recursive call. loginAtempt is: " + loginAtempt + ", pass value: " + pass);
      if (loginAtempt.startsWith(pass)) {
      
        res.add(pass);
        visited.add(loginAtempt);
        
        String subLoginAtempt = loginAtempt.substring(pass.length());
//        System.out.println(tab + "This is the " + level + " recursive call. SUBLoginAtempt after cut: " + subLoginAtempt);
        if (crack(subLoginAtempt, passwords, res, level + 1)) return true;
        res.remove(res.size()-1);//remove last item
      } 
    }
    //if run here mean in loginAtempt has one word is not key
    return false;
    
  }
  
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int t = in.nextInt();
    for (int i = 0; i < t;i++) {
      visited.clear();
      int n = in.nextInt();
      String[] passwords = new String[n];
      for(int j = 0; j < n; j++) {
        passwords[j] = in.next();
      }
      String loginAtempt = in.next();
      List<String> res = new ArrayList<>();
      crack(loginAtempt, passwords, res, 1);
      if (res.isEmpty()) {
        System.out.println("WRONG PASSWORD");
      } else {
        res.stream().forEach(e -> {System.out.print(e + " ");});
        System.out.println("");
      }
    }
    in.close();
  }
  
  
}
