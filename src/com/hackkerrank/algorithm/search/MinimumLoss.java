package com.hackkerrank.algorithm.search;

import java.util.Scanner;

/**
 * we will sort the array and then using the 2 pointer approach
 * remember that we only calculate for min if using past year price - future year price
 * 
 * for example: 20 7 8 2 5
 * 
 * then after we sort we have: 2 5 7 8 20 
 * 
 * @author alex
 *
 */
public class MinimumLoss {
  
  private static void merge(long[][] leftArr, long[][] rightArr, long[][] arr) {
    int i = 0, j = 0, k = 0;
    while (i < leftArr.length && j < rightArr.length) {
      if (leftArr[i][1] < rightArr[j][1]) {
        arr[k] = leftArr[i];
        i++;
      } else {
        arr[k] = rightArr[j];
        j++;
      }
      k++;
    }
    while (i < leftArr.length) {
      arr[k] = leftArr[i];
      i++;k++;
    }
    while (j < rightArr.length) {
      arr[k] = rightArr[j];
      j++;k++;
    }
  }
  
  private static void mergeSort(long[][] arr) {
    if (arr.length < 2) return;
    int mid = arr.length / 2;
    long[][] leftArr = new long[mid][];
    long[][] rightArr = new long[arr.length - mid][];
    for (int i = 0; i < mid;i++) {
      leftArr[i] = arr[i];
    }
    for(int i = mid; i < arr.length; i++) {
      rightArr[i-mid] = arr[i];
    }
    mergeSort(leftArr);
    mergeSort(rightArr);
    merge(leftArr, rightArr, arr);
  }
  
  
  /**
   * the value will be read to long[][] with format:
   * 1st element is index
   * 2nd element is value
   * 
   * e.g: 20 7 8 2 5 will have long[][] is [[0,20], [1,7],[2,8], [3,2], [4,5]]
   * @param args
   */
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    long[][] pVals = new long[n][2];
    for(int i = 0; i < n;i++) {
      pVals[i][0] = i;
      pVals[i][1] = in.nextLong();
    }
    
    mergeSort(pVals);
    
    int i = 0, j = 1;
    long min = (long) Math.pow(10, 16);
    // after sort, we only do the min calculation if the index of bigger value is smaller than
    // smaller value, so we don't make 5 - 2 because one of rule is the sell value smaller than buy value
    // 2 5 7 8 20
    // 3 4 1 2 0
    while (j < n) {
      if (pVals[j][0] < pVals[i][0]) {
        long newMin = pVals[j][1] - pVals[i][1];
        if (newMin < min) min = newMin;
      }
      i++;j++;
    }
    
    System.out.print(min);
  }
  
}
