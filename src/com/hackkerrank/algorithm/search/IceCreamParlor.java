package com.hackkerrank.algorithm.search;

import java.util.Scanner;

public class IceCreamParlor {
  
  private static void merge(int[][] leftArr, int[][] rightArr, int[][] arr) {
    int i = 0, j = 0, k = 0;
    while (i < leftArr.length && j < rightArr.length) {
      if (leftArr[i][0] < rightArr[j][0]) {
        arr[k] = leftArr[i];
        i++;
      } else {
        arr[k] = rightArr[j];
        j++;
      }
      k++;
    }
    while (i < leftArr.length) {
      arr[k] = leftArr[i];
      i++;k++;
    }
    while (j < rightArr.length) {
      arr[k] = rightArr[j];
      j++;k++;
    }
  }
  
  private static void mergeSort(int[][] arr) {
    if (arr.length < 2) return;
    int mid = arr.length / 2;
    int[][] leftArr = new int[mid][];
    int[][] rightArr = new int[arr.length - mid][];
    for (int i = 0; i < mid;i++) {
      leftArr[i] = arr[i];
    }
    for(int i = mid; i < arr.length; i++) {
      rightArr[i-mid] = arr[i];
    }
    mergeSort(leftArr);
    mergeSort(rightArr);
    merge(leftArr, rightArr, arr);
  }
  
  private static void printIceCream(int m, int[][] costs) {
    mergeSort(costs);
    for(int i = 0; i < costs.length - 1; i++) {
      for (int j = i + 1; j < costs.length; j++) {
        if (costs[i][1] + costs[j][1] == m) {
          System.out.println(costs[i][0] + " " + costs[j][0]);
        }
      }
    }
  }
  
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int trip = in.nextInt();
    for (int i = 0; i < trip; i++) {
      int m = in.nextInt();
      int n = in.nextInt();
      int[][] costs = new int[n][n];
      for (int j = 0; j< n; j++) {
        costs[j][0] = j+1;
        costs[j][1] = in.nextInt();
      }
      
      printIceCream(m, costs);
    }
    in.close();
  }
}
