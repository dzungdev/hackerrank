package com.hackkerrank.algorithm.search;

import java.util.Scanner;

public class SherlockAndArray {
  
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int testCasesNum = in.nextInt();
    for (int i = 0; i < testCasesNum;i++) {
      
      
      int n = in.nextInt();
      String answer = "NO";
        int[] arr = new int[n];
        long sum = 0;
        for (int j = 0; j < n;j++) {
          arr[j] = in.nextInt();
          sum += arr[j];
        }
        if (n == 1) {
          answer = "YES";
        } else {
          long leftSum = arr[0];
          for(int j = 1; j < n;j++) {
            if ((sum - arr[j] - leftSum) == leftSum) {
              answer = "YES";
              break;
            } else {
              leftSum += arr[j];
            }
          }
        }
      
      
      System.out.println(answer);
    }
    in.close();
  }
  
}
