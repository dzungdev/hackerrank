package com.hackkerrank.algorithm.search;

import java.util.Scanner;

public class RecursiveDigitSum {
  
  static long sumDigit(String n) {
    String[] a = n.split("\\B+");
    long sum = 0;
    for(String s : a) sum += Long.valueOf(s);
    return sum < 10 ? sum : sumDigit(Long.toString(sum));
    }
  
  
  
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    String n = in.next();
    int k = in.nextInt();
    in.close();
    
    String[] arr = n.split("");
    long firstSum = 0;
    for (String s:arr) firstSum += Long.valueOf(s);
    firstSum *= k;
    
    System.out.print(sumDigit(Long.toString(firstSum)));
  }
  
}
