package com.hackkerrank.algorithm.search;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MissingNumbers {

  private static void swap(long[] arr, int source, int dest) {
    long sourceVal = arr[source];
    arr[source] = arr[dest];
    arr[dest] = sourceVal;
  }

  private static int partition(long[] arr, int start, int end) {
    // use last element is pivot
    long pivot = arr[end];
    int i = start; // Index of of smaller element

    for (int j = start; j < end; j++) {
      if (arr[j] < pivot) {
        swap(arr, i, j);
        i++;
      }
    }
    // swap the pivot value to position after last smaller element
    swap(arr, i, end);
    return i;
  }

  private static void quicksort(long[] arr, int start, int end) {
    if (start < end) {

      int pivotIndex = partition(arr, start, end);
      quicksort(arr, start, pivotIndex - 1);
      quicksort(arr, pivotIndex + 1, end);
    }
  }
  
  public static void main(String[] args) {
    Map<Long, Long> mapA = new HashMap<>();
    Map<Long, Long> mapB = new HashMap<>();
    Scanner in = new Scanner(System.in);
    long n = in.nextLong();
    for (long i = 0; i < n; i++) {
      long val = in.nextLong();
      if (mapA.containsKey(val)) {
        mapA.put(val, mapA.get(val)+1);
      } else {
        mapA.put(val, 1L);
      }
    }
    long m = in.nextLong();
    for (long i = 0; i < m; i++) {
      long val = in.nextLong();
      if (mapB.containsKey(val)) {
        mapB.put(val, mapB.get(val)+1);
      } else {
        mapB.put(val, 1L);
      }
    }
    
    long[] bvals = new long[mapB.keySet().size()];
    
    int i = 0;
    for(long val: mapB.keySet()) {
      bvals[i++] = val;
    }
    quicksort(bvals, 0, bvals.length-1);
    
    for (Long val: bvals) {
      
      long leftOutNum = mapA.containsKey(val) ? mapB.get(val) - mapA.get(val) : mapB.get(val);
//      System.out.println("val is "+ val + " has leftOutNum is " + leftOutNum);
      if (leftOutNum > 0) {
        System.out.print(val + " ");
      }
    }
  }
  
}
