package com.hackkerrank.algorithm.search;

import java.util.Scanner;

public class HackerlandRadioTransmitters {
  
  //7 2
  //9 5 4 2 6 15 12
  
  
  
  private static void merge(int[] leftArr, int[] rightArr, int[] arr) {
    int i = 0,j = 0,k = 0; 
    while (i < leftArr.length && j < rightArr.length) {
      if (leftArr[i] <= rightArr[j]) {
        arr[k] = leftArr[i];
        i++;
      } else {
        arr[k] = rightArr[j];
        j++;
      }
      k++;
    }
    while (i < leftArr.length) {
      arr[k] = leftArr[i];
      i++;k++;
    }
    while (j < rightArr.length) {
      arr[k] = rightArr[j];
      j++;k++;
    }
  }
  
  private static void mergeSort(int[] arr) {
    if (arr.length < 2) return;//if the arr is 1 element, then sorted and return
    int mid = arr.length / 2;
    int[] leftArr = new int[mid];
    int[] rightArr = new int[arr.length - mid];
    for (int i = 0; i < mid; i++) {
      leftArr[i] = arr[i];
    }
    for (int i = mid; i < arr.length; i++) {
      rightArr[i-mid] = arr[i];
    }
    mergeSort(leftArr);
    mergeSort(rightArr);
    merge(leftArr, rightArr, arr);
  }
  
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int k = in.nextInt();
    int[] arr = new int[n];
    for(int x_i=0; x_i < n; x_i++){
        arr[x_i] = in.nextInt();
    }
    
    mergeSort(arr);
    int i = 0;
    int totalTransmitter = 0;
    while (i < arr.length) {
      totalTransmitter++;
      int loc = arr[i] +  2 * k;
      while (i < n && arr[i] <= loc) i++;
//      loc = arr[--i] + k;
//      while (i < n && arr[i] <= loc) i++;
    }
    System.out.print(i);
    
    System.out.println(totalTransmitter);
    
  }
  
}
