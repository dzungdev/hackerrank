package com.hackkerrank.algorithm.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class GridlandMetro {

  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);

    long rows = scan.nextInt();
    long cols = scan.nextInt();
    long k = scan.nextLong();

    // Simple entry is c1 and c2 value, c1 is key, c2 is value.
    Map<Long, List<List<Long>>> trainMap = new HashMap<>();
    for (long i = 0; i < k; i++) {
      long row = scan.nextLong();
      long newC1 = scan.nextLong();
      long newC2 = scan.nextLong();

      if (trainMap.containsKey(row)) {
        
        List<List<Long>> trains = trainMap.get(row);
        for (List<Long> train : trains) {
          long existedC1 = train.get(0);
          long existedC2 = train.get(1);
          if (newC2 < existedC1 || newC1 > existedC2) {
            List<Long> newTrain = new ArrayList<>();
            newTrain.add(newC1);
            newTrain.add(newC2);
            trains.add(newTrain);
            break;
          } else {
            if (newC2 > existedC2) train.set(1, newC2);
            if (newC1 < existedC1) train.set(0, newC1);
          }
        }
      } else {
        List<List<Long>> trains = new ArrayList<>();
        List<Long> firstTrain = new ArrayList();
        firstTrain.add(newC1);
        firstTrain.add(newC2);
        trains.add(firstTrain);
        trainMap.put(row, trains);
      }
    }

    long totalOccupiedTrainTrack = 0;
    for (long rowth : trainMap.keySet()) {
      List<List<Long>> trains = trainMap.get(rowth);
      for (List<Long> train : trains) {
        totalOccupiedTrainTrack += (train.get(1) - train.get(0) + 1);
      }
    }

    System.out.print(rows * cols - totalOccupiedTrainTrack);
  }

}
