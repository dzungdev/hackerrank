package com.hackkerrank.ds.arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DynamicArray {
  
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int lastAnswer = 0;
    int n = in.nextInt();
    int q = in.nextInt();
    
    List<List<Integer>> seqList = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      seqList.add(new ArrayList<>());
    }
    for(int i = 0; i < q; i++) {
      int type = in.nextInt();
      int x = in.nextInt();
      int y = in.nextInt();
      
      int index = (x ^ lastAnswer) % n;
      List<Integer> seq = seqList.get(index);
      if (type == 1) {
        seq.add(y);  
      } else {
        lastAnswer = seq.get(y % seq.size());
        System.out.println(lastAnswer);
      }
    }
    in.close();
  }
  
  
}
